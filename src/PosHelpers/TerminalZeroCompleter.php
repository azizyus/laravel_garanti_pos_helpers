<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 26.11.2018
 * Time: 10:32
 */

namespace App\PosHelpers;


class TerminalZeroCompleter
{


    public static function complete($strTerminalID_)
    {


        $missingTotalNumber = 9 - strlen($strTerminalID_);
        $zeros = "";
        for ($i = (9 - $missingTotalNumber); $i < 9; $i++) {

            $zeros .= "0";
        }

        return $zeros.$strTerminalID_;

    }

}