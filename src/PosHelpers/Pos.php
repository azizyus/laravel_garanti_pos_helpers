<?php
/**
 * Created by PhpStorm.
 * User: can
 * Date: 25.02.2018
 * Time: 18:59
 */

namespace App\PosHelpers;




use App\CreditCardHelpers\CreditCard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;

class Pos implements IPos
{




    public $strMode = "PROD";
    public $strAmount = "";
    public $price;
    public $creditCard;
    public $email;
    public $postUrl;
    public $currencyCode;


    public $terminalId;
    public $terminalIdCompletedByZerosToNineDigit;
    public $provautAccountName;
    public $provacutPassword;
    public $merchantId;
    public $errorEmail;

    public function __construct()
    {

    }

    public function getPostUrl()
    {
        return $this->postUrl;
    }

    public function getExtraHtml()
    {
        return "";
    }





    public function sendErrorMail(AbstractPosResult $posResult)
    {


        PosErrorMail::sendErrorMail($posResult,$this->errorEmail);


    }


    public function setTerminalId($terminalId)
    {


        $this->terminalId = $terminalId;
        $this->terminalIdCompletedByZerosToNineDigit = TerminalZeroCompleter::complete($terminalId);

    }

    public function getTerminalId()
    {
        return $this->terminalId;
    }


    public function setPrice($price)
    {
        $this->strAmount = PosPricePurifier::calculate($price);
    }

    public function getPrice()
    {
        return $this->strAmount;
    }
    public function makeCreditCard(Request $request) : CreditCard
    {
        $name = $request->get("cardName");
        $cardNumber = $request->get("cardnumber");
        $month = $request->get("cardexpiredatemonth");
        $year = $request->get("cardexpiredateyear");
        $ccv = $request->get("cardcvv2");

        return new CreditCard($name,$cardNumber,$month,$year,$ccv);

    }

    public function handleBankRequest(Request $request) : AbstractPosResult
    {


        $creditCard = $this->makeCreditCard($request);
        $currencyCode = $this->currencyCode;
        $posPrice = $this->getPrice();


        $email = $this->email;
        $ip = \Illuminate\Support\Facades\Request::ip();
        ########


    ######### XML REQUEST #########

        $strMode = $this->strMode;
        $strVersion = "v0.01";
        $strTerminalID = $this->terminalId;
        $strTerminalID_ = $this->terminalIdCompletedByZerosToNineDigit; //TerminalID baþýna 0 eklenerek 9 digite tamamlanmalýdýr.
        $strProvUserID = $this->provautAccountName;
        $strProvisionPassword = $this->provacutPassword; //ProvUserID þifresi
        $strUserID = $this->provautAccountName;
        $strMerchantID = $this->merchantId; //Üye Ýþyeri Numarasý
        $strIPAddress = $ip;  //Müþteri IP si
        $strEmailAddress = $email;
        $strOrderID = uniqid();
        $strInstallmentCnt = ""; //Taksit Sayýsý. Boþ gönderilirse taksit yapýlmaz
        $strNumber = $creditCard->number; //cardnumber
        $strExpireDate = $creditCard->month.$creditCard->year;
        $strCVV2 = $creditCard->ccv;
        $strAmount = $posPrice; //Ýþlem Tutarý 1.00 TL için 100 gönderilmelidir.
        $strType = "sales";
        $strCurrencyCode = $currencyCode;
        $strCardholderPresentCode = "0";
        $strMotoInd = "N";
        $strHostAddress = "https://sanalposprov.garanti.com.tr/VPServlet";
        $SecurityData = strtoupper(sha1($strProvisionPassword.$strTerminalID_));
        $HashData = strtoupper(sha1($strOrderID.$strTerminalID.$strNumber.$strAmount.$SecurityData));
        $xml= "
<?xml version=\"1.0\" encoding=\"UTF-8\"?>
        <GVPSRequest>
            <Mode>$strMode</Mode>
            <Version>$strVersion</Version>
        <Terminal>
            <ProvUserID>$strProvUserID</ProvUserID>
            <HashData>$HashData</HashData>
            <UserID>$strUserID</UserID>
            <ID>$strTerminalID</ID>
            <MerchantID>$strMerchantID</MerchantID>
        </Terminal>
        <Customer>
            <IPAddress>$strIPAddress</IPAddress>
            <EmailAddress>$strEmailAddress</EmailAddress>
        </Customer>
        <Card>
            <Number>$strNumber</Number>
            <ExpireDate>$strExpireDate</ExpireDate>
            <CVV2>$strCVV2</CVV2>
        </Card>
        <Order>
                <OrderID>$strOrderID</OrderID>
                <GroupID></GroupID>
            <AddressList><Address>
                <Type>S</Type>
                <Name></Name>
                <LastName></LastName>
                <Company></Company>
                <Text></Text>
                <District></District>
                <City></City>
                <PostalCode></PostalCode>
                <Country></Country>
                <PhoneNumber></PhoneNumber>
                </Address>
            </AddressList>
        </Order>
            <Transaction>
                <Type>$strType</Type>
                <InstallmentCnt>$strInstallmentCnt</InstallmentCnt>
                <Amount>$strAmount</Amount>
                <CurrencyCode>$strCurrencyCode</CurrencyCode>
                <CardholderPresentCode>$strCardholderPresentCode</CardholderPresentCode>
                <MotoInd>$strMotoInd</MotoInd>
                <Description></Description>
                <OriginalRetrefNum></OriginalRetrefNum>
            </Transaction>
        </GVPSRequest>";


        $ch=curl_init();
        curl_setopt($ch, CURLOPT_URL, $strHostAddress);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1) ;
        curl_setopt($ch, CURLOPT_POSTFIELDS, "data=".$xml);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $results = curl_exec($ch);
        curl_close($ch);


//        echo "<b>Giden İstek </b><br />";
//        return dd($xml);
//        echo "<br /><b>Gelen Yanıt </b><br />";
//        echo    htmlentities($results)."<br>";
        $xml_parser = xml_parser_create();
        xml_parse_into_struct($xml_parser,$results,$vals,$index);
        xml_parser_free($xml_parser);

    ######### XML REQUEST #########
        //Sadece ReasonCode deðerini alýyoruz.

        ####### POS RESULTING ########
            $posResult = new PosResult($results,false);
        try
        {
            $strReasonCodeValue = $vals[$index['REASONCODE'][0]]['value'];

            if($strReasonCodeValue == "00") //its done
            {

                $posResult = new PosResult($results,true);

            }
            else
            {

                $posResult = new PosResult($results,false);

            }
        }
        catch (\Exception $exception)
        {





        }
        finally
        {
           if(!$posResult->getSuccess()) $this->sendErrorMail($posResult);
        }

        return $posResult;
        ####### POS RESULTING ########

        }


}