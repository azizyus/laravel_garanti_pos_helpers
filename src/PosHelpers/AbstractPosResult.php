<?php
/**
 * Created by PhpStorm.
 * User: can_r
 * Date: 14/4/2018
 * Time: 4:39 PM
 */

namespace App\PosHelpers;


Abstract class AbstractPosResult
{

    protected $result;
    protected $success;
    public $errorMessage;
    public $email;
    public $ip;

    public $sysErrorMessage;
    public $cardNumber;
    public $cardHolderName;
    public $terminalId;
    public $merchantId;
    public $rawXmlResult;
    public $exceptionMessage;

    abstract public function __construct($result,Bool $success,$userErrorMessage = "Daha Sonra Tekrar Deneyiniz");

    abstract  public function getErrorMessage();

    abstract  public function getResult();

    abstract public function getSuccess() : Bool;

}