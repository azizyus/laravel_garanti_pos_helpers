<?php
/**
 * Created by PhpStorm.
 * User: can
 * Date: 25.02.2018
 * Time: 20:11
 */

namespace App\PosHelpers;


class D3PosResult extends AbstractPosResult
{




    public function __construct($result,Bool $success,$userErrorMessage = "Daha Sonra Tekrar Deneyiniz")
    {


        $this->rawXmlResult = $result->all();
        $this->result = $result;

        try
        {


        $this->email = $result->email;
        $this->ip = $result->customeripaddress;
        $this->errorMessage = $result->hostmsg;
        $this->sysErrorMessage = $result->mderrormessage;
        $this->cardNumber = $result->MaskedPan;
        $this->cardHolderName = $result->cardholder;
        $this->terminalId = $result->terminalid;
        $this->merchantId = $result->terminalmerchantid;
        }
        catch (\Exception $exception)
        {

            $this->errorMessage = $userErrorMessage." (E54)";
            $this->exceptionMessage = $exception;


        }

        $this->success = $success;



    }

    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    public function getResult()
    {

        return $this->result;

    }

    public function getSuccess() : bool
    {

        return $this->success;

    }




}