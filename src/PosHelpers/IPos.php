<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 25.11.2018
 * Time: 23:59
 */

namespace App\PosHelpers;


interface IPos
{


    public function getPostUrl();

    public function getExtraHtml();
}