<?php
/**
 * Created by PhpStorm.
 * User: can
 * Date: 25.02.2018
 * Time: 20:11
 */

namespace App\PosHelpers;


class PosResult extends AbstractPosResult
{



    public function __construct($result,Bool $success,$userErrorMessage = "Daha Sonra Tekrar Deneyiniz")
    {




        $result =   $results = simplexml_load_string($result); //xml -> class
        $this->rawXmlResult = $result;
        $this->result = $result;

        try
        {


        $this->email = $result->Customer->EmailAddress;
        $this->ip = $result->Customer->IPAddress;
        $this->errorMessage = $result->Transaction->Response->ErrorMsg;
        $this->sysErrorMessage = $result->Transaction->Response->SysErrMsg;
        $this->cardNumber = $result->Transaction->CardNumberMasked;
        $this->cardHolderName = $result->Transaction->CardHolderName;
        $this->terminalId = $result->Terminal->ID;
        $this->merchantId = $result->Terminal->MerchantID;
        }
        catch (\Exception $exception)
        {

            $this->errorMessage = $userErrorMessage." (E3D)";
            $this->exceptionMessage = $exception;


        }

        $this->success = $success;



    }

    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    public function getResult()
    {

        return $this->result;

    }

    public function getSuccess() : bool
    {

        return $this->success;

    }




}