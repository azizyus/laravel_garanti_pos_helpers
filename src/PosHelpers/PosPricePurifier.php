<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 26.11.2018
 * Time: 18:26
 */

namespace App\PosHelpers;


class PosPricePurifier
{

    public static function calculate($price)
    {


        $price = $price * 100;

        $posFormatted = number_format($price,0,"","");
        return $posFormatted;


    }


}