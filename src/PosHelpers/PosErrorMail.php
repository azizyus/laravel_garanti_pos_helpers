<?php
/**
 * Created by PhpStorm.
 * User: can_r
 * Date: 14/4/2018
 * Time: 4:57 PM
 */

namespace App\PosHelpers;


use App\PosHelpers\PosErrorNotification;

use const Grpc\OP_RECV_INITIAL_METADATA;
use Illuminate\Support\Facades\Notification;

class PosErrorMail
{

    public static function sendErrorMail(AbstractPosResult $posResult,$errorMail)
    {
        Notification::route("mail",$errorMail)->notify((new PosErrorNotification($posResult)));

    }

}