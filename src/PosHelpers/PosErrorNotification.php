<?php

namespace App\PosHelpers;

use App\PosHelpers\AbstractPosResult;
use App\PosHelpers\PosResult;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Mail;

class PosErrorNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $posResult;
    public function __construct(AbstractPosResult $posResult)
    {
        //

        $this->posResult = $posResult;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        $posResult = $this->posResult;

         $mail = new MailMessage();

        $data=[

            "email"=>$posResult->email,
            "ip"=>$posResult->ip,
            "ErrorMsg"=>$posResult->errorMessage,
            "SysErrMsg"=>$posResult->sysErrorMessage,
            "CardNumberMasked"=>$posResult->cardNumber,
            "CardHolderName"=>$posResult->cardHolderName,
            "terminalId"=>$posResult->terminalId,
            "merchantId"=>$posResult->merchantId,
            "exceptionMessage"=>$posResult->exceptionMessage,

        ];


        ###### DEBUG FILE STRING CALC ####
        $debugString="";
        foreach ($posResult->rawXmlResult as $key => $message)
        {
            $debugString.= "$key : $message   \n";
        }
        ###### DEBUG FILE STRING CALC ####

        $mail = $mail->attachData($debugString,"result.txt");


        $mail = $mail->view("email.custom_payment.error",$data);

        return $mail;


    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
