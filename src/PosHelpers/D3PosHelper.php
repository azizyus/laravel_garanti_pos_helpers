<?php
/**
 * Created by PhpStorm.
 * User: can_r
 * Date: 14/4/2018
 * Time: 9:18 AM
 */

namespace App\PosHelpers;

use Illuminate\Http\Request;

class D3PosHelper implements IPos
{

    public $strMode = "PROD";
    public $strApiVersion = "v0.01";
    public $strTerminalProvUserID = "PROVAUT";
    public $strType = "sales";
    public $strAmount = ""; //İşlem Tutarı  1.00 TL için 100 gönderilmeli
    public $strCurrencyCode = "";
    public $strInstallmentCount = ""; //Taksit Sayısı. Boş gönderilirse taksit yapılmaz
    public $strTerminalUserID = "PROVAUT";
    public $strOrderID = "DENEME";  //her işlemde farklı bir değer gönderilmeli
    public $strCustomeripaddress = "127.0.0.1";
    public $strcustomeremailaddress = "eticaret@garanti.com.tr";
    public $strTerminalIDRaw = null;
    public $strTerminalIDCompletedToNineDigitWithZeros = null;

    public $strTerminalMerchantID = null; //Üye İşyeri Numarası
    public $strStoreKey = null; //3D Secure şifreniz
    public $strProvisionPassword = null; //TerminalProvUserID şifresi
    public $strSuccessURL = "";
    public $strErrorURL = "";
    public $SecurityData = null; //set it on constructor
    public $HashData = null; //set it on constructor


    private $successUrl = null;
    private $errorUrl = null;

    public $postLink = "https://sanalposprov.garanti.com.tr/servlet/gt3dengine";

    public $HashDataWithoutSha1;
    public $securiyDataWithoutSha1;
    public function __construct()
    {





        ## GET LOCAL IP ##

        $ipAddress = \Illuminate\Support\Facades\Request::ip();
        if ($ipAddress == "::1") $ipAddress = "127.0.0.1";
        //it must be unique
        $this->strOrderID = uniqid();




    }


    public function getSuccessUrl()
    {

        return $this->successUrl;
    }

    public function getErrorUrl()
    {
        return $this->errorUrl;
    }

    public function setPrice($price)
    {
        $this->strAmount = PosPricePurifier::calculate($price);
    }

    public function getPrice()
    {
        return $this->strAmount;
    }

    public function setTerminalId($strTerminalID)
    {
        ### IM ADDING MISSING 0 NUMBERS ###

        $this->strTerminalIDRaw = $strTerminalID;
        $this->strTerminalIDCompletedToNineDigitWithZeros = TerminalZeroCompleter::complete($strTerminalID);

    }

    public function getTerminalId()
    {
        return $this->strTerminalIDRaw;
    }


    public function setMerchantId($merchantId)
    {
        $this->strTerminalMerchantID = $merchantId;
    }
    public function getMerchantId()
    {
        return $this->strTerminalMerchantID;
    }

    public function get3DPosLink()
    {

        return $this->postLink;
    }

    public function getExtraHtml()
    {
        return $this->getRenderedPosInputs();
    }

    public function calculateHash()
    {
        $SecurityDataWithoutSha1 = $this->strProvisionPassword . $this->strTerminalIDCompletedToNineDigitWithZeros;
        $SecurityData = sha1($this->strProvisionPassword . $this->strTerminalIDCompletedToNineDigitWithZeros);
        $SecurityData = strtoupper($SecurityData); //IT SHOULD BE UPPER OTHERWISE DOESNT GET SHA1'D PROPERLY
        $this->SecurityData  = $SecurityData;
        $HashDataWithoutSha1 = ($this->strTerminalIDRaw . $this->strOrderID . $this->strAmount . $this->strSuccessURL . $this->strErrorURL . $this->strType . $this->strInstallmentCount . $this->strStoreKey . $this->SecurityData);
        $HashData = sha1($HashDataWithoutSha1);
        $HashData = strtoupper($HashData);
        $this->HashData = $HashData;

        $this->HashDataWithoutSha1 = $HashDataWithoutSha1;
        $this->securiyDataWithoutSha1 = $SecurityDataWithoutSha1;

    }

    public function getPostUrl()
    {
        return $this->get3DPosLink();
    }

    public function getRenderedPosInputs()
    {





        $data = [

            "strMode" => $this->strMode,
            "strApiVersion" => $this->strApiVersion,
            "strTerminalProvUserID" => $this->strTerminalProvUserID,
            "strTerminalUserID" => $this->strTerminalUserID,
            "strTerminalMerchantID" => $this->strTerminalMerchantID,
            "strType" => $this->strType,
            "strAmount" => $this->strAmount,
            "strCurrencyCode" => $this->strCurrencyCode,
            "strInstallmentCount" => $this->strInstallmentCount,
            "strOrderID" => $this->strOrderID,
            "strTerminalID" => $this->strTerminalIDRaw,
            "strSuccessURL" => $this->strSuccessURL,
            "strErrorURL" => $this->strErrorURL,
            "strcustomeremailaddress" => $this->strcustomeremailaddress,
            "strCustomeripaddress" => $this->strCustomeripaddress,
            "HashData" => $this->HashData,
            "secure3dsecuritylevel" => "3D_PAY"


        ];



        //   dd($data);

        $view = view("pos.3d_pos_inputs")->with($data)->render();

        //it will return html codes of inputs
        return $view;

    }


    public function handleBankRequest(Request $request) : AbstractPosResult
    {
        $mdStatus = $request->mdstatus;
        $responseHashparams = $request->hashparams;
        $responseHash = $request->hash;

        $result = $request->all();

        //this shit should check is request comin from  bank but i dont even know working or not
        $isThisRequestFromBank = $this->hash_data($request, $responseHashparams, $responseHash);

        if ($mdStatus == "1" && $isThisRequestFromBank) //yep ist fucking string 1
        {

            $posResult = new D3PosResult($request,true);
            return $posResult;
        }


        $posResult = new D3PosResult($request,false);
        return $posResult;

    }

    function hash_data($result, $responseHashparams, $responseHash)
    {
        $isValidHash = false;
        $storekey = $this->strStoreKey;
        if ($responseHashparams !== NULL && $responseHashparams !== "") {
            $digestData = "";
            $paramList = explode(":", $responseHashparams);
            foreach ($paramList as $param) {
                if (isset($result[strtolower($param)])) {
                    $value = $result[strtolower($param)];
                    if ($value == null) {
                        $value = "";
                    }
                    $digestData .= $value;
                }
            }
            $digestData .= $storekey;
            $hashCalculated = base64_encode(pack('H*', sha1($digestData)));
            if ($responseHash == $hashCalculated) {
                $isValidHash = true;
            }
        }
        return $isValidHash;
    }

}