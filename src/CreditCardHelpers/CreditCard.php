<?php
/**
 * Created by PhpStorm.
 * User: can
 * Date: 25.02.2018
 * Time: 02:53
 */

namespace App\CreditCardHelpers;


use Illuminate\Support\Str;

class CreditCard
{


    public $name;
    public $number;
    public $month;
    public $year;
    public $ccv;

    public function __construct($name,$number,$month,$year,$ccv)
    {


        $this->name = $name;
        $this->number = preg_replace("/\s+/","",$number);
        $this->month = $month;
        $this->year = $year;
        $this->ccv = $ccv;

    }


    public function getBin()
    {


        $cardNumber = $this->number;

        $cardNumber = Str::substr($cardNumber,0,6);


        return $cardNumber;

    }


}